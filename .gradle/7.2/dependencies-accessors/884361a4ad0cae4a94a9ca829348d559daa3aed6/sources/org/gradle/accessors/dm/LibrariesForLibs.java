package org.gradle.accessors.dm;

import org.gradle.api.NonNullApi;
import org.gradle.api.artifacts.MinimalExternalModuleDependency;
import org.gradle.plugin.use.PluginDependency;
import org.gradle.api.artifacts.ExternalModuleDependencyBundle;
import org.gradle.api.artifacts.MutableVersionConstraint;
import org.gradle.api.provider.Provider;
import org.gradle.api.provider.ProviderFactory;
import org.gradle.api.internal.catalog.AbstractExternalDependencyFactory;
import org.gradle.api.internal.catalog.DefaultVersionCatalog;
import java.util.Map;
import javax.inject.Inject;

/**
 * A catalog of dependencies accessible via the `libs` extension.
*/
@NonNullApi
public class LibrariesForLibs extends AbstractExternalDependencyFactory {

    private final AbstractExternalDependencyFactory owner = this;
    private final FeignLibraryAccessors laccForFeignLibraryAccessors = new FeignLibraryAccessors(owner);
    private final FlywayLibraryAccessors laccForFlywayLibraryAccessors = new FlywayLibraryAccessors(owner);
    private final LogstashLibraryAccessors laccForLogstashLibraryAccessors = new LogstashLibraryAccessors(owner);
    private final MockitoLibraryAccessors laccForMockitoLibraryAccessors = new MockitoLibraryAccessors(owner);
    private final R2dbcLibraryAccessors laccForR2dbcLibraryAccessors = new R2dbcLibraryAccessors(owner);
    private final ReactorLibraryAccessors laccForReactorLibraryAccessors = new ReactorLibraryAccessors(owner);
    private final SleuthLibraryAccessors laccForSleuthLibraryAccessors = new SleuthLibraryAccessors(owner);
    private final SpringLibraryAccessors laccForSpringLibraryAccessors = new SpringLibraryAccessors(owner);
    private final SpringdocLibraryAccessors laccForSpringdocLibraryAccessors = new SpringdocLibraryAccessors(owner);
    private final TestcontainersLibraryAccessors laccForTestcontainersLibraryAccessors = new TestcontainersLibraryAccessors(owner);
    private final VersionAccessors vaccForVersionAccessors = new VersionAccessors(providers, config);
    private final BundleAccessors baccForBundleAccessors = new BundleAccessors(providers, config);
    private final PluginAccessors paccForPluginAccessors = new PluginAccessors(providers, config);

    @Inject
    public LibrariesForLibs(DefaultVersionCatalog config, ProviderFactory providers) {
        super(config, providers);
    }

    /**
     * Returns the group of libraries at feign
     */
    public FeignLibraryAccessors getFeign() { return laccForFeignLibraryAccessors; }

    /**
     * Returns the group of libraries at flyway
     */
    public FlywayLibraryAccessors getFlyway() { return laccForFlywayLibraryAccessors; }

    /**
     * Returns the group of libraries at logstash
     */
    public LogstashLibraryAccessors getLogstash() { return laccForLogstashLibraryAccessors; }

    /**
     * Returns the group of libraries at mockito
     */
    public MockitoLibraryAccessors getMockito() { return laccForMockitoLibraryAccessors; }

    /**
     * Returns the group of libraries at r2dbc
     */
    public R2dbcLibraryAccessors getR2dbc() { return laccForR2dbcLibraryAccessors; }

    /**
     * Returns the group of libraries at reactor
     */
    public ReactorLibraryAccessors getReactor() { return laccForReactorLibraryAccessors; }

    /**
     * Returns the group of libraries at sleuth
     */
    public SleuthLibraryAccessors getSleuth() { return laccForSleuthLibraryAccessors; }

    /**
     * Returns the group of libraries at spring
     */
    public SpringLibraryAccessors getSpring() { return laccForSpringLibraryAccessors; }

    /**
     * Returns the group of libraries at springdoc
     */
    public SpringdocLibraryAccessors getSpringdoc() { return laccForSpringdocLibraryAccessors; }

    /**
     * Returns the group of libraries at testcontainers
     */
    public TestcontainersLibraryAccessors getTestcontainers() { return laccForTestcontainersLibraryAccessors; }

    /**
     * Returns the group of versions at versions
     */
    public VersionAccessors getVersions() { return vaccForVersionAccessors; }

    /**
     * Returns the group of bundles at bundles
     */
    public BundleAccessors getBundles() { return baccForBundleAccessors; }

    /**
     * Returns the group of plugins at plugins
     */
    public PluginAccessors getPlugins() { return paccForPluginAccessors; }

    public static class FeignLibraryAccessors extends SubDependencyFactory {
        private final FeignReactorLibraryAccessors laccForFeignReactorLibraryAccessors = new FeignReactorLibraryAccessors(owner);

        public FeignLibraryAccessors(AbstractExternalDependencyFactory owner) { super(owner); }

        /**
         * Returns the group of libraries at feign.reactor
         */
        public FeignReactorLibraryAccessors getReactor() { return laccForFeignReactorLibraryAccessors; }

    }

    public static class FeignReactorLibraryAccessors extends SubDependencyFactory {
        private final FeignReactorSpringLibraryAccessors laccForFeignReactorSpringLibraryAccessors = new FeignReactorSpringLibraryAccessors(owner);

        public FeignReactorLibraryAccessors(AbstractExternalDependencyFactory owner) { super(owner); }

            /**
             * Creates a dependency provider for core (com.playtika.reactivefeign:feign-reactor-core)
             * This dependency was declared in catalog com.parity:dependency-versions-module:0.0.4
             */
            public Provider<MinimalExternalModuleDependency> getCore() { return create("feign.reactor.core"); }

            /**
             * Creates a dependency provider for webclient (com.playtika.reactivefeign:feign-reactor-webclient)
             * This dependency was declared in catalog com.parity:dependency-versions-module:0.0.4
             */
            public Provider<MinimalExternalModuleDependency> getWebclient() { return create("feign.reactor.webclient"); }

        /**
         * Returns the group of libraries at feign.reactor.spring
         */
        public FeignReactorSpringLibraryAccessors getSpring() { return laccForFeignReactorSpringLibraryAccessors; }

    }

    public static class FeignReactorSpringLibraryAccessors extends SubDependencyFactory {

        public FeignReactorSpringLibraryAccessors(AbstractExternalDependencyFactory owner) { super(owner); }

            /**
             * Creates a dependency provider for configuration (com.playtika.reactivefeign:feign-reactor-spring-configuration)
             * This dependency was declared in catalog com.parity:dependency-versions-module:0.0.4
             */
            public Provider<MinimalExternalModuleDependency> getConfiguration() { return create("feign.reactor.spring.configuration"); }

    }

    public static class FlywayLibraryAccessors extends SubDependencyFactory {

        public FlywayLibraryAccessors(AbstractExternalDependencyFactory owner) { super(owner); }

            /**
             * Creates a dependency provider for core (org.flywaydb:flyway-core)
             * This dependency was declared in catalog com.parity:dependency-versions-module:0.0.4
             */
            public Provider<MinimalExternalModuleDependency> getCore() { return create("flyway.core"); }

    }

    public static class LogstashLibraryAccessors extends SubDependencyFactory {
        private final LogstashLogbackLibraryAccessors laccForLogstashLogbackLibraryAccessors = new LogstashLogbackLibraryAccessors(owner);

        public LogstashLibraryAccessors(AbstractExternalDependencyFactory owner) { super(owner); }

        /**
         * Returns the group of libraries at logstash.logback
         */
        public LogstashLogbackLibraryAccessors getLogback() { return laccForLogstashLogbackLibraryAccessors; }

    }

    public static class LogstashLogbackLibraryAccessors extends SubDependencyFactory {

        public LogstashLogbackLibraryAccessors(AbstractExternalDependencyFactory owner) { super(owner); }

            /**
             * Creates a dependency provider for encoder (net.logstash.logback:logstash-logback-encoder)
             * This dependency was declared in catalog com.parity:dependency-versions-module:0.0.4
             */
            public Provider<MinimalExternalModuleDependency> getEncoder() { return create("logstash.logback.encoder"); }

    }

    public static class MockitoLibraryAccessors extends SubDependencyFactory {

        public MockitoLibraryAccessors(AbstractExternalDependencyFactory owner) { super(owner); }

            /**
             * Creates a dependency provider for kotlin (org.mockito.kotlin:mockito-kotlin)
             * This dependency was declared in catalog com.parity:dependency-versions-module:0.0.4
             */
            public Provider<MinimalExternalModuleDependency> getKotlin() { return create("mockito.kotlin"); }

    }

    public static class R2dbcLibraryAccessors extends SubDependencyFactory {

        public R2dbcLibraryAccessors(AbstractExternalDependencyFactory owner) { super(owner); }

            /**
             * Creates a dependency provider for postgresql (io.r2dbc:r2dbc-postgresql)
             * This dependency was declared in catalog com.parity:dependency-versions-module:0.0.4
             */
            public Provider<MinimalExternalModuleDependency> getPostgresql() { return create("r2dbc.postgresql"); }

    }

    public static class ReactorLibraryAccessors extends SubDependencyFactory {

        public ReactorLibraryAccessors(AbstractExternalDependencyFactory owner) { super(owner); }

            /**
             * Creates a dependency provider for kafka (io.projectreactor.kafka:reactor-kafka)
             * This dependency was declared in catalog com.parity:dependency-versions-module:0.0.4
             */
            public Provider<MinimalExternalModuleDependency> getKafka() { return create("reactor.kafka"); }

    }

    public static class SleuthLibraryAccessors extends SubDependencyFactory {

        public SleuthLibraryAccessors(AbstractExternalDependencyFactory owner) { super(owner); }

            /**
             * Creates a dependency provider for zipkin (org.springframework.cloud:spring-cloud-sleuth-zipkin)
             * This dependency was declared in catalog com.parity:dependency-versions-module:0.0.4
             */
            public Provider<MinimalExternalModuleDependency> getZipkin() { return create("sleuth.zipkin"); }

    }

    public static class SpringLibraryAccessors extends SubDependencyFactory {
        private final SpringCloudLibraryAccessors laccForSpringCloudLibraryAccessors = new SpringCloudLibraryAccessors(owner);

        public SpringLibraryAccessors(AbstractExternalDependencyFactory owner) { super(owner); }

            /**
             * Creates a dependency provider for boot (org.springframework.boot:spring-boot-dependencies)
             * This dependency was declared in catalog com.parity:dependency-versions-module:0.0.4
             */
            public Provider<MinimalExternalModuleDependency> getBoot() { return create("spring.boot"); }

            /**
             * Creates a dependency provider for kafka (org.springframework.kafka:spring-kafka)
             * This dependency was declared in catalog com.parity:dependency-versions-module:0.0.4
             */
            public Provider<MinimalExternalModuleDependency> getKafka() { return create("spring.kafka"); }

            /**
             * Creates a dependency provider for metrics (org.springframework.metrics:spring-metrics)
             * This dependency was declared in catalog com.parity:dependency-versions-module:0.0.4
             */
            public Provider<MinimalExternalModuleDependency> getMetrics() { return create("spring.metrics"); }

        /**
         * Returns the group of libraries at spring.cloud
         */
        public SpringCloudLibraryAccessors getCloud() { return laccForSpringCloudLibraryAccessors; }

    }

    public static class SpringCloudLibraryAccessors extends SubDependencyFactory {
        private final SpringCloudStarterLibraryAccessors laccForSpringCloudStarterLibraryAccessors = new SpringCloudStarterLibraryAccessors(owner);

        public SpringCloudLibraryAccessors(AbstractExternalDependencyFactory owner) { super(owner); }

        /**
         * Returns the group of libraries at spring.cloud.starter
         */
        public SpringCloudStarterLibraryAccessors getStarter() { return laccForSpringCloudStarterLibraryAccessors; }

    }

    public static class SpringCloudStarterLibraryAccessors extends SubDependencyFactory {

        public SpringCloudStarterLibraryAccessors(AbstractExternalDependencyFactory owner) { super(owner); }

            /**
             * Creates a dependency provider for sleuth (org.springframework.cloud:spring-cloud-starter-sleuth)
             * This dependency was declared in catalog com.parity:dependency-versions-module:0.0.4
             */
            public Provider<MinimalExternalModuleDependency> getSleuth() { return create("spring.cloud.starter.sleuth"); }

    }

    public static class SpringdocLibraryAccessors extends SubDependencyFactory {
        private final SpringdocOpenapiLibraryAccessors laccForSpringdocOpenapiLibraryAccessors = new SpringdocOpenapiLibraryAccessors(owner);

        public SpringdocLibraryAccessors(AbstractExternalDependencyFactory owner) { super(owner); }

        /**
         * Returns the group of libraries at springdoc.openapi
         */
        public SpringdocOpenapiLibraryAccessors getOpenapi() { return laccForSpringdocOpenapiLibraryAccessors; }

    }

    public static class SpringdocOpenapiLibraryAccessors extends SubDependencyFactory {
        private final SpringdocOpenapiWebfluxLibraryAccessors laccForSpringdocOpenapiWebfluxLibraryAccessors = new SpringdocOpenapiWebfluxLibraryAccessors(owner);

        public SpringdocOpenapiLibraryAccessors(AbstractExternalDependencyFactory owner) { super(owner); }

            /**
             * Creates a dependency provider for kotlin (org.springdoc:springdoc-openapi-kotlin)
             * This dependency was declared in catalog com.parity:dependency-versions-module:0.0.4
             */
            public Provider<MinimalExternalModuleDependency> getKotlin() { return create("springdoc.openapi.kotlin"); }

        /**
         * Returns the group of libraries at springdoc.openapi.webflux
         */
        public SpringdocOpenapiWebfluxLibraryAccessors getWebflux() { return laccForSpringdocOpenapiWebfluxLibraryAccessors; }

    }

    public static class SpringdocOpenapiWebfluxLibraryAccessors extends SubDependencyFactory {

        public SpringdocOpenapiWebfluxLibraryAccessors(AbstractExternalDependencyFactory owner) { super(owner); }

            /**
             * Creates a dependency provider for ui (org.springdoc:springdoc-openapi-webflux-ui)
             * This dependency was declared in catalog com.parity:dependency-versions-module:0.0.4
             */
            public Provider<MinimalExternalModuleDependency> getUi() { return create("springdoc.openapi.webflux.ui"); }

    }

    public static class TestcontainersLibraryAccessors extends SubDependencyFactory {

        public TestcontainersLibraryAccessors(AbstractExternalDependencyFactory owner) { super(owner); }

            /**
             * Creates a dependency provider for postgresql (org.testcontainers:postgresql)
             * This dependency was declared in catalog com.parity:dependency-versions-module:0.0.4
             */
            public Provider<MinimalExternalModuleDependency> getPostgresql() { return create("testcontainers.postgresql"); }

    }

    public static class VersionAccessors extends VersionFactory  {

        private final FeignVersionAccessors vaccForFeignVersionAccessors = new FeignVersionAccessors(providers, config);
        private final FlywayVersionAccessors vaccForFlywayVersionAccessors = new FlywayVersionAccessors(providers, config);
        private final KtlintVersionAccessors vaccForKtlintVersionAccessors = new KtlintVersionAccessors(providers, config);
        private final LogstashVersionAccessors vaccForLogstashVersionAccessors = new LogstashVersionAccessors(providers, config);
        private final MockitoVersionAccessors vaccForMockitoVersionAccessors = new MockitoVersionAccessors(providers, config);
        private final R2dbcVersionAccessors vaccForR2dbcVersionAccessors = new R2dbcVersionAccessors(providers, config);
        private final ReactorVersionAccessors vaccForReactorVersionAccessors = new ReactorVersionAccessors(providers, config);
        private final SleuthVersionAccessors vaccForSleuthVersionAccessors = new SleuthVersionAccessors(providers, config);
        private final SpringVersionAccessors vaccForSpringVersionAccessors = new SpringVersionAccessors(providers, config);
        private final SpringdocVersionAccessors vaccForSpringdocVersionAccessors = new SpringdocVersionAccessors(providers, config);
        private final TestVersionAccessors vaccForTestVersionAccessors = new TestVersionAccessors(providers, config);
        private final TestcontainersVersionAccessors vaccForTestcontainersVersionAccessors = new TestcontainersVersionAccessors(providers, config);
        public VersionAccessors(ProviderFactory providers, DefaultVersionCatalog config) { super(providers, config); }

            /**
             * Returns the version associated to this alias: kotlin (1.5.30)
             * If the version is a rich version and that its not expressible as a
             * single version string, then an empty string is returned.
             * This version was declared in catalog com.parity:dependency-versions-module:0.0.4
             */
            public Provider<String> getKotlin() { return getVersion("kotlin"); }

            /**
             * Returns the version associated to this alias: researchgate (2.8.1)
             * If the version is a rich version and that its not expressible as a
             * single version string, then an empty string is returned.
             * This version was declared in catalog com.parity:dependency-versions-module:0.0.4
             */
            public Provider<String> getResearchgate() { return getVersion("researchgate"); }

            /**
             * Returns the version associated to this alias: swagger (3.0.0)
             * If the version is a rich version and that its not expressible as a
             * single version string, then an empty string is returned.
             * This version was declared in catalog com.parity:dependency-versions-module:0.0.4
             */
            public Provider<String> getSwagger() { return getVersion("swagger"); }

        /**
         * Returns the group of versions at versions.feign
         */
        public FeignVersionAccessors getFeign() { return vaccForFeignVersionAccessors; }

        /**
         * Returns the group of versions at versions.flyway
         */
        public FlywayVersionAccessors getFlyway() { return vaccForFlywayVersionAccessors; }

        /**
         * Returns the group of versions at versions.ktlint
         */
        public KtlintVersionAccessors getKtlint() { return vaccForKtlintVersionAccessors; }

        /**
         * Returns the group of versions at versions.logstash
         */
        public LogstashVersionAccessors getLogstash() { return vaccForLogstashVersionAccessors; }

        /**
         * Returns the group of versions at versions.mockito
         */
        public MockitoVersionAccessors getMockito() { return vaccForMockitoVersionAccessors; }

        /**
         * Returns the group of versions at versions.r2dbc
         */
        public R2dbcVersionAccessors getR2dbc() { return vaccForR2dbcVersionAccessors; }

        /**
         * Returns the group of versions at versions.reactor
         */
        public ReactorVersionAccessors getReactor() { return vaccForReactorVersionAccessors; }

        /**
         * Returns the group of versions at versions.sleuth
         */
        public SleuthVersionAccessors getSleuth() { return vaccForSleuthVersionAccessors; }

        /**
         * Returns the group of versions at versions.spring
         */
        public SpringVersionAccessors getSpring() { return vaccForSpringVersionAccessors; }

        /**
         * Returns the group of versions at versions.springdoc
         */
        public SpringdocVersionAccessors getSpringdoc() { return vaccForSpringdocVersionAccessors; }

        /**
         * Returns the group of versions at versions.test
         */
        public TestVersionAccessors getTest() { return vaccForTestVersionAccessors; }

        /**
         * Returns the group of versions at versions.testcontainers
         */
        public TestcontainersVersionAccessors getTestcontainers() { return vaccForTestcontainersVersionAccessors; }

    }

    public static class FeignVersionAccessors extends VersionFactory  {

        public FeignVersionAccessors(ProviderFactory providers, DefaultVersionCatalog config) { super(providers, config); }

            /**
             * Returns the version associated to this alias: feign.reactor (3.0.3)
             * If the version is a rich version and that its not expressible as a
             * single version string, then an empty string is returned.
             * This version was declared in catalog com.parity:dependency-versions-module:0.0.4
             */
            public Provider<String> getReactor() { return getVersion("feign.reactor"); }

    }

    public static class FlywayVersionAccessors extends VersionFactory  {

        public FlywayVersionAccessors(ProviderFactory providers, DefaultVersionCatalog config) { super(providers, config); }

            /**
             * Returns the version associated to this alias: flyway.core (7.15.0)
             * If the version is a rich version and that its not expressible as a
             * single version string, then an empty string is returned.
             * This version was declared in catalog com.parity:dependency-versions-module:0.0.4
             */
            public Provider<String> getCore() { return getVersion("flyway.core"); }

    }

    public static class KtlintVersionAccessors extends VersionFactory  {

        public KtlintVersionAccessors(ProviderFactory providers, DefaultVersionCatalog config) { super(providers, config); }

            /**
             * Returns the version associated to this alias: ktlint.detekt (1.17.1)
             * If the version is a rich version and that its not expressible as a
             * single version string, then an empty string is returned.
             * This version was declared in catalog com.parity:dependency-versions-module:0.0.4
             */
            public Provider<String> getDetekt() { return getVersion("ktlint.detekt"); }

    }

    public static class LogstashVersionAccessors extends VersionFactory  {

        private final LogstashLogbackVersionAccessors vaccForLogstashLogbackVersionAccessors = new LogstashLogbackVersionAccessors(providers, config);
        public LogstashVersionAccessors(ProviderFactory providers, DefaultVersionCatalog config) { super(providers, config); }

        /**
         * Returns the group of versions at versions.logstash.logback
         */
        public LogstashLogbackVersionAccessors getLogback() { return vaccForLogstashLogbackVersionAccessors; }

    }

    public static class LogstashLogbackVersionAccessors extends VersionFactory  {

        public LogstashLogbackVersionAccessors(ProviderFactory providers, DefaultVersionCatalog config) { super(providers, config); }

            /**
             * Returns the version associated to this alias: logstash.logback.encoder (6.3)
             * If the version is a rich version and that its not expressible as a
             * single version string, then an empty string is returned.
             * This version was declared in catalog com.parity:dependency-versions-module:0.0.4
             */
            public Provider<String> getEncoder() { return getVersion("logstash.logback.encoder"); }

    }

    public static class MockitoVersionAccessors extends VersionFactory  {

        public MockitoVersionAccessors(ProviderFactory providers, DefaultVersionCatalog config) { super(providers, config); }

            /**
             * Returns the version associated to this alias: mockito.kotlin (3.2.0)
             * If the version is a rich version and that its not expressible as a
             * single version string, then an empty string is returned.
             * This version was declared in catalog com.parity:dependency-versions-module:0.0.4
             */
            public Provider<String> getKotlin() { return getVersion("mockito.kotlin"); }

    }

    public static class R2dbcVersionAccessors extends VersionFactory  {

        private final R2dbcPostgreVersionAccessors vaccForR2dbcPostgreVersionAccessors = new R2dbcPostgreVersionAccessors(providers, config);
        public R2dbcVersionAccessors(ProviderFactory providers, DefaultVersionCatalog config) { super(providers, config); }

        /**
         * Returns the group of versions at versions.r2dbc.postgre
         */
        public R2dbcPostgreVersionAccessors getPostgre() { return vaccForR2dbcPostgreVersionAccessors; }

    }

    public static class R2dbcPostgreVersionAccessors extends VersionFactory  {

        public R2dbcPostgreVersionAccessors(ProviderFactory providers, DefaultVersionCatalog config) { super(providers, config); }

            /**
             * Returns the version associated to this alias: r2dbc.postgre.sql (0.8.8.RELEASE)
             * If the version is a rich version and that its not expressible as a
             * single version string, then an empty string is returned.
             * This version was declared in catalog com.parity:dependency-versions-module:0.0.4
             */
            public Provider<String> getSql() { return getVersion("r2dbc.postgre.sql"); }

    }

    public static class ReactorVersionAccessors extends VersionFactory  {

        public ReactorVersionAccessors(ProviderFactory providers, DefaultVersionCatalog config) { super(providers, config); }

            /**
             * Returns the version associated to this alias: reactor.kafka (1.3.6)
             * If the version is a rich version and that its not expressible as a
             * single version string, then an empty string is returned.
             * This version was declared in catalog com.parity:dependency-versions-module:0.0.4
             */
            public Provider<String> getKafka() { return getVersion("reactor.kafka"); }

    }

    public static class SleuthVersionAccessors extends VersionFactory  {

        public SleuthVersionAccessors(ProviderFactory providers, DefaultVersionCatalog config) { super(providers, config); }

            /**
             * Returns the version associated to this alias: sleuth.zipkin (3.0.4)
             * If the version is a rich version and that its not expressible as a
             * single version string, then an empty string is returned.
             * This version was declared in catalog com.parity:dependency-versions-module:0.0.4
             */
            public Provider<String> getZipkin() { return getVersion("sleuth.zipkin"); }

    }

    public static class SpringVersionAccessors extends VersionFactory  {

        private final SpringCloudVersionAccessors vaccForSpringCloudVersionAccessors = new SpringCloudVersionAccessors(providers, config);
        private final SpringDependencyVersionAccessors vaccForSpringDependencyVersionAccessors = new SpringDependencyVersionAccessors(providers, config);
        public SpringVersionAccessors(ProviderFactory providers, DefaultVersionCatalog config) { super(providers, config); }

            /**
             * Returns the version associated to this alias: spring.boot (2.5.5)
             * If the version is a rich version and that its not expressible as a
             * single version string, then an empty string is returned.
             * This version was declared in catalog com.parity:dependency-versions-module:0.0.4
             */
            public Provider<String> getBoot() { return getVersion("spring.boot"); }

            /**
             * Returns the version associated to this alias: spring.kafka (2.7.7)
             * If the version is a rich version and that its not expressible as a
             * single version string, then an empty string is returned.
             * This version was declared in catalog com.parity:dependency-versions-module:0.0.4
             */
            public Provider<String> getKafka() { return getVersion("spring.kafka"); }

            /**
             * Returns the version associated to this alias: spring.metrics (0.5.1.RELEASE)
             * If the version is a rich version and that its not expressible as a
             * single version string, then an empty string is returned.
             * This version was declared in catalog com.parity:dependency-versions-module:0.0.4
             */
            public Provider<String> getMetrics() { return getVersion("spring.metrics"); }

        /**
         * Returns the group of versions at versions.spring.cloud
         */
        public SpringCloudVersionAccessors getCloud() { return vaccForSpringCloudVersionAccessors; }

        /**
         * Returns the group of versions at versions.spring.dependency
         */
        public SpringDependencyVersionAccessors getDependency() { return vaccForSpringDependencyVersionAccessors; }

    }

    public static class SpringCloudVersionAccessors extends VersionFactory  {

        private final SpringCloudStarterVersionAccessors vaccForSpringCloudStarterVersionAccessors = new SpringCloudStarterVersionAccessors(providers, config);
        public SpringCloudVersionAccessors(ProviderFactory providers, DefaultVersionCatalog config) { super(providers, config); }

        /**
         * Returns the group of versions at versions.spring.cloud.starter
         */
        public SpringCloudStarterVersionAccessors getStarter() { return vaccForSpringCloudStarterVersionAccessors; }

    }

    public static class SpringCloudStarterVersionAccessors extends VersionFactory  {

        public SpringCloudStarterVersionAccessors(ProviderFactory providers, DefaultVersionCatalog config) { super(providers, config); }

            /**
             * Returns the version associated to this alias: spring.cloud.starter.sleuth (3.0.4)
             * If the version is a rich version and that its not expressible as a
             * single version string, then an empty string is returned.
             * This version was declared in catalog com.parity:dependency-versions-module:0.0.4
             */
            public Provider<String> getSleuth() { return getVersion("spring.cloud.starter.sleuth"); }

    }

    public static class SpringDependencyVersionAccessors extends VersionFactory  {

        public SpringDependencyVersionAccessors(ProviderFactory providers, DefaultVersionCatalog config) { super(providers, config); }

            /**
             * Returns the version associated to this alias: spring.dependency.management (1.0.11.RELEASE)
             * If the version is a rich version and that its not expressible as a
             * single version string, then an empty string is returned.
             * This version was declared in catalog com.parity:dependency-versions-module:0.0.4
             */
            public Provider<String> getManagement() { return getVersion("spring.dependency.management"); }

    }

    public static class SpringdocVersionAccessors extends VersionFactory  {

        public SpringdocVersionAccessors(ProviderFactory providers, DefaultVersionCatalog config) { super(providers, config); }

            /**
             * Returns the version associated to this alias: springdoc.openapi (1.4.3)
             * If the version is a rich version and that its not expressible as a
             * single version string, then an empty string is returned.
             * This version was declared in catalog com.parity:dependency-versions-module:0.0.4
             */
            public Provider<String> getOpenapi() { return getVersion("springdoc.openapi"); }

    }

    public static class TestVersionAccessors extends VersionFactory  {

        public TestVersionAccessors(ProviderFactory providers, DefaultVersionCatalog config) { super(providers, config); }

            /**
             * Returns the version associated to this alias: test.containers (1.16.0)
             * If the version is a rich version and that its not expressible as a
             * single version string, then an empty string is returned.
             * This version was declared in catalog com.parity:dependency-versions-module:0.0.4
             */
            public Provider<String> getContainers() { return getVersion("test.containers"); }

    }

    public static class TestcontainersVersionAccessors extends VersionFactory  {

        public TestcontainersVersionAccessors(ProviderFactory providers, DefaultVersionCatalog config) { super(providers, config); }

            /**
             * Returns the version associated to this alias: testcontainers.postgresql (1.16.0)
             * If the version is a rich version and that its not expressible as a
             * single version string, then an empty string is returned.
             * This version was declared in catalog com.parity:dependency-versions-module:0.0.4
             */
            public Provider<String> getPostgresql() { return getVersion("testcontainers.postgresql"); }

    }

    public static class BundleAccessors extends BundleFactory {
        private final FeignBundleAccessors baccForFeignBundleAccessors = new FeignBundleAccessors(providers, config);
        private final SpringdocBundleAccessors baccForSpringdocBundleAccessors = new SpringdocBundleAccessors(providers, config);

        public BundleAccessors(ProviderFactory providers, DefaultVersionCatalog config) { super(providers, config); }

        /**
         * Returns the group of bundles at bundles.feign
         */
        public FeignBundleAccessors getFeign() { return baccForFeignBundleAccessors; }

        /**
         * Returns the group of bundles at bundles.springdoc
         */
        public SpringdocBundleAccessors getSpringdoc() { return baccForSpringdocBundleAccessors; }

    }

    public static class FeignBundleAccessors extends BundleFactory {

        public FeignBundleAccessors(ProviderFactory providers, DefaultVersionCatalog config) { super(providers, config); }

            /**
             * Creates a dependency bundle provider for feign.client which is an aggregate for the following dependencies:
             * <ul>
             *    <li>com.playtika.reactivefeign:feign-reactor-core</li>
             *    <li>com.playtika.reactivefeign:feign-reactor-webclient</li>
             *    <li>com.playtika.reactivefeign:feign-reactor-spring-configuration</li>
             * </ul>
             * This bundle was declared in catalog com.parity:dependency-versions-module:0.0.4
             */
            public Provider<ExternalModuleDependencyBundle> getClient() { return createBundle("feign.client"); }

    }

    public static class SpringdocBundleAccessors extends BundleFactory {

        public SpringdocBundleAccessors(ProviderFactory providers, DefaultVersionCatalog config) { super(providers, config); }

            /**
             * Creates a dependency bundle provider for springdoc.openapi which is an aggregate for the following dependencies:
             * <ul>
             *    <li>org.springdoc:springdoc-openapi-webflux-ui</li>
             *    <li>org.springdoc:springdoc-openapi-kotlin</li>
             * </ul>
             * This bundle was declared in catalog com.parity:dependency-versions-module:0.0.4
             */
            public Provider<ExternalModuleDependencyBundle> getOpenapi() { return createBundle("springdoc.openapi"); }

    }

    public static class PluginAccessors extends PluginFactory {
        private final JvmPluginAccessors baccForJvmPluginAccessors = new JvmPluginAccessors(providers, config);
        private final KtlintPluginAccessors baccForKtlintPluginAccessors = new KtlintPluginAccessors(providers, config);
        private final SpringPluginAccessors baccForSpringPluginAccessors = new SpringPluginAccessors(providers, config);

        public PluginAccessors(ProviderFactory providers, DefaultVersionCatalog config) { super(providers, config); }

            /**
             * Creates a plugin provider for researchgate to the plugin id 'net.researchgate.release'
             * This plugin was declared in catalog com.parity:dependency-versions-module:0.0.4
             */
            public Provider<PluginDependency> getResearchgate() { return createPlugin("researchgate"); }

        /**
         * Returns the group of bundles at plugins.jvm
         */
        public JvmPluginAccessors getJvm() { return baccForJvmPluginAccessors; }

        /**
         * Returns the group of bundles at plugins.ktlint
         */
        public KtlintPluginAccessors getKtlint() { return baccForKtlintPluginAccessors; }

        /**
         * Returns the group of bundles at plugins.spring
         */
        public SpringPluginAccessors getSpring() { return baccForSpringPluginAccessors; }

    }

    public static class JvmPluginAccessors extends PluginFactory {

        public JvmPluginAccessors(ProviderFactory providers, DefaultVersionCatalog config) { super(providers, config); }

            /**
             * Creates a plugin provider for jvm.kotlin to the plugin id 'jvm'
             * This plugin was declared in catalog com.parity:dependency-versions-module:0.0.4
             */
            public Provider<PluginDependency> getKotlin() { return createPlugin("jvm.kotlin"); }

    }

    public static class KtlintPluginAccessors extends PluginFactory {

        public KtlintPluginAccessors(ProviderFactory providers, DefaultVersionCatalog config) { super(providers, config); }

            /**
             * Creates a plugin provider for ktlint.detekt to the plugin id 'io.gitlab.arturbosch.detekt'
             * This plugin was declared in catalog com.parity:dependency-versions-module:0.0.4
             */
            public Provider<PluginDependency> getDetekt() { return createPlugin("ktlint.detekt"); }

    }

    public static class SpringPluginAccessors extends PluginFactory {
        private final SpringDependencyPluginAccessors baccForSpringDependencyPluginAccessors = new SpringDependencyPluginAccessors(providers, config);

        public SpringPluginAccessors(ProviderFactory providers, DefaultVersionCatalog config) { super(providers, config); }

            /**
             * Creates a plugin provider for spring.boot to the plugin id 'org.springframework.boot'
             * This plugin was declared in catalog com.parity:dependency-versions-module:0.0.4
             */
            public Provider<PluginDependency> getBoot() { return createPlugin("spring.boot"); }

            /**
             * Creates a plugin provider for spring.kotlin to the plugin id 'plugin.spring'
             * This plugin was declared in catalog com.parity:dependency-versions-module:0.0.4
             */
            public Provider<PluginDependency> getKotlin() { return createPlugin("spring.kotlin"); }

        /**
         * Returns the group of bundles at plugins.spring.dependency
         */
        public SpringDependencyPluginAccessors getDependency() { return baccForSpringDependencyPluginAccessors; }

    }

    public static class SpringDependencyPluginAccessors extends PluginFactory {

        public SpringDependencyPluginAccessors(ProviderFactory providers, DefaultVersionCatalog config) { super(providers, config); }

            /**
             * Creates a plugin provider for spring.dependency.management to the plugin id 'io.spring.dependency-management'
             * This plugin was declared in catalog com.parity:dependency-versions-module:0.0.4
             */
            public Provider<PluginDependency> getManagement() { return createPlugin("spring.dependency.management"); }

    }

}
