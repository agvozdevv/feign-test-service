@Suppress("DSL_SCOPE_VIOLATION")
plugins {
    kotlin("plugin.spring")
    alias(libs.plugins.spring.boot)
}

dependencies {
    implementation(kotlin("stdlib"))
    implementation(kotlin("reflect"))

    implementation("com.parity:card-realms-gateway-api:0.0.65")
    implementation("com.n2.cards:card-products-api:0.0.91")
    implementation("com.parity:cards-anti-fraud-api:0.0.50")

    implementation("org.springframework.boot:spring-boot-starter-webflux")
    implementation("com.fasterxml.jackson.module:jackson-module-kotlin")

    implementation(libs.bundles.feign.client)
    implementation(libs.logstash.logback.encoder)
}
