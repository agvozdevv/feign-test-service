package com.n2.cards.feigntest.service.cardsantifraud.api

import com.n2.cards.antifraud.model.ProcessingResult
import reactor.core.publisher.Mono

interface TransactionProcessorService {
    fun fraudCheck(request: String): Mono<ProcessingResult>
}
