package com.n2.cards.feigntest.controller.cardrealmsgateway

import com.n2.cards.cardrealmsgateway.model.cardssca.RequestDto
import com.n2.cards.cardrealmsgateway.model.cardssca.ThreeDSAuthenticationResultRequest
import com.n2.cards.cardrealmsgateway.model.cardssca.ThreeDSAuthenticationResultResponse
import com.n2.cards.feigntest.service.cardrealmsgateway.api.CardsScaApiService
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PutMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestHeader
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono

@RestController
class CardsScaApiController(private val cardsScaApiService: CardsScaApiService) {

    @GetMapping("/cards-sca/requests")
    fun getRequestsForCardIdList(
        @RequestHeader("realm-token") realmToken: String,
        @RequestParam cardIdList: List<String>
    ): Flux<RequestDto> {
        return cardsScaApiService.getRequestsForCardIdList(realmToken, cardIdList)
    }

    @PutMapping("/cards-sca/requests/{requestId}/authentication-result")
    fun confirmRequest(
        @PathVariable requestId: String,
        @RequestHeader("realm-token") realmToken: String,
        @RequestBody request: ThreeDSAuthenticationResultRequest
    ): Mono<ThreeDSAuthenticationResultResponse> {
        return cardsScaApiService.confirmRequest(requestId, realmToken, request)
    }
}
