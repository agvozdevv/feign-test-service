package com.n2.cards.feigntest.service.cardsantifraud.impl

import com.n2.cards.antifraud.feign.IssuerFraudConstraintApi
import com.n2.cards.antifraud.model.constraint.dto.IssuerFraudConstraintDto
import com.n2.cards.feigntest.service.cardsantifraud.api.IssuerFraudConstraintsService
import org.springframework.stereotype.Service
import reactor.core.publisher.Mono

@Service
class IssuerFraudConstraintsServiceImpl(
    private val issuerFraudConstraintApi: IssuerFraudConstraintApi
) : IssuerFraudConstraintsService {
    override fun addIssuerFraudConstraint(constraint: IssuerFraudConstraintDto): Mono<IssuerFraudConstraintDto> {
        return issuerFraudConstraintApi.addIssuerFraudConstraint(constraint)
    }

    override fun getLatestIssuerFraudConstraint(): Mono<IssuerFraudConstraintDto> {
        return issuerFraudConstraintApi.getLatestIssuerFraudConstraint()
    }
}
