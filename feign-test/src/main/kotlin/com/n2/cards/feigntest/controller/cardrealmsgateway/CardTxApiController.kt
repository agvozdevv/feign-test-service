package com.n2.cards.feigntest.controller.cardrealmsgateway

import com.n2.cards.cardrealmsgateway.model.cardtx.TransactionDto
import com.n2.cards.feigntest.service.cardrealmsgateway.api.CardTxApiService
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestHeader
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController
import reactor.core.publisher.Mono

@RestController
class CardTxApiController(private val cardTxApiService: CardTxApiService) {

    @GetMapping("/card-tx/transactions")
    fun getTransactions(
        @RequestHeader("realm-token") realmToken: String,
        @RequestParam transactionIdList: List<String>
    ): Mono<List<TransactionDto>> {
        return cardTxApiService.getTransactions(realmToken, transactionIdList)
    }

    @GetMapping("/card-tx/transactions/{transactionId}")
    fun getTransactionById(
        @PathVariable("transactionId") transactionId: String,
        @RequestHeader("realm-token") realmToken: String
    ): Mono<TransactionDto> {
        return cardTxApiService.getTransactionById(transactionId, realmToken)
    }
}
