package com.n2.cards.feigntest.service.cardrealmsgateway.api

import com.n2.cards.cardrealmsgateway.model.cardblocking.CardBlockingRequestGateway
import com.n2.cards.cardrealmsgateway.model.cardblocking.CardBlockingResponseGateway
import reactor.core.publisher.Mono

interface CardBlockingApiService {
    fun blockCard(cardId: String, realmToken: String, request: CardBlockingRequestGateway): Mono<CardBlockingResponseGateway>
}
