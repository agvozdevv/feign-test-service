package com.n2.cards.feigntest.service.cardsantifraud.api

import com.n2.cards.antifraud.model.constraint.dto.IssuerFraudConstraintDto
import reactor.core.publisher.Mono

interface IssuerFraudConstraintsService {
    fun addIssuerFraudConstraint(constraint: IssuerFraudConstraintDto): Mono<IssuerFraudConstraintDto>

    fun getLatestIssuerFraudConstraint(): Mono<IssuerFraudConstraintDto>
}
