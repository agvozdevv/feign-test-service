package com.n2.cards.feigntest.service.cardrealmsgateway.impl

import com.n2.cards.cardrealmsgateway.feign.CardsScaApi
import com.n2.cards.cardrealmsgateway.model.cardssca.RequestDto
import com.n2.cards.cardrealmsgateway.model.cardssca.ThreeDSAuthenticationResultRequest
import com.n2.cards.cardrealmsgateway.model.cardssca.ThreeDSAuthenticationResultResponse
import com.n2.cards.feigntest.service.cardrealmsgateway.api.CardsScaApiService
import org.springframework.stereotype.Service
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono

@Service
class CardsScaApiServiceImpl(private val cardsScaApi: CardsScaApi) : CardsScaApiService {
    override fun getRequestsForCardIdList(realmToken: String, cardIdList: List<String>): Flux<RequestDto> {
        return cardsScaApi.getRequestsForCardIdList(realmToken, cardIdList)
    }

    override fun confirmRequest(
        requestId: String,
        realmToken: String,
        request: ThreeDSAuthenticationResultRequest
    ): Mono<ThreeDSAuthenticationResultResponse> {
        return cardsScaApi.confirmRequest(requestId, realmToken, request)
    }
}
