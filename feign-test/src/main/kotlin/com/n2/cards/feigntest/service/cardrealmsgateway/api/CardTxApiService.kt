package com.n2.cards.feigntest.service.cardrealmsgateway.api

import com.n2.cards.cardrealmsgateway.model.cardtx.TransactionDto
import reactor.core.publisher.Mono

interface CardTxApiService {
    fun getTransactions(realmToken: String, transactionIdList: List<String>): Mono<List<TransactionDto>>

    fun getTransactionById(transactionId: String, realmToken: String): Mono<TransactionDto>
}
