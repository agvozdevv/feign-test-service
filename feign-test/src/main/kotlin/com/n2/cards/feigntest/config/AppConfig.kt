package com.n2.cards.feigntest.config

import org.springframework.context.annotation.Configuration
import reactivefeign.spring.config.EnableReactiveFeignClients

@EnableReactiveFeignClients(
    clients = [
        com.n2.cards.cardrealmsgateway.feign.CardBlockingApi::class,
        com.n2.cards.cardrealmsgateway.feign.CardDetailsApi::class,
        com.n2.cards.cardrealmsgateway.feign.CardIssuingApi::class,
        com.n2.cards.cardrealmsgateway.feign.CardsScaApi::class,
        com.n2.cards.cardrealmsgateway.feign.CardTransactionApi::class,
        com.n2.cards.cardproducts.feign.CardProductApi::class,
        com.n2.cards.antifraud.feign.IssuerFraudConstraintApi::class,
        com.n2.cards.antifraud.feign.MccBlacklistApi::class,
        com.n2.cards.antifraud.feign.MerchantBlacklistApi::class,
        com.n2.cards.antifraud.feign.NetworkFraudConstraintApi::class,
        com.n2.cards.antifraud.feign.TransactionProcessorApi::class
    ]
)
@Configuration
class AppConfig
