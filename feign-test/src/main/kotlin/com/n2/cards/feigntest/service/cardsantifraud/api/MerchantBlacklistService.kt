package com.n2.cards.feigntest.service.cardsantifraud.api

import com.n2.cards.antifraud.model.blacklist.MerchantBlacklistPageDto
import com.n2.cards.antifraud.model.blacklist.dto.MerchantBlacklistDto
import reactor.core.publisher.Mono

interface MerchantBlacklistService {
    fun addMerchantToBlacklist(merchant: MerchantBlacklistDto): Mono<MerchantBlacklistDto>

    fun deleteMerchantFromBlacklist(id: String): Mono<Int>

    fun getMerchantFromBlacklist(id: String): Mono<MerchantBlacklistDto>

    fun getMerchantsFromBlacklistWithPagination(count: Int, page: Int): Mono<MerchantBlacklistPageDto>
}
