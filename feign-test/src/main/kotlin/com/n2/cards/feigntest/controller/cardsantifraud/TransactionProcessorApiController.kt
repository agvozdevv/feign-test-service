package com.n2.cards.feigntest.controller.cardsantifraud

import com.n2.cards.antifraud.model.ProcessingResult
import com.n2.cards.feigntest.service.cardsantifraud.api.TransactionProcessorService
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RestController
import reactor.core.publisher.Mono

@RestController
class TransactionProcessorApiController(private val transactionProcessorService: TransactionProcessorService) {
    @PostMapping("/antifraud/transaction")
    fun fraudCheck(@RequestBody request: String): Mono<ProcessingResult> {
        return transactionProcessorService.fraudCheck(request)
    }
}
