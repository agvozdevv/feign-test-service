package com.n2.cards.feigntest.service.cardsantifraud.api

import com.n2.cards.antifraud.model.blacklist.MccBlacklistPageDto
import com.n2.cards.antifraud.model.blacklist.dto.MccBlacklistDto
import reactor.core.publisher.Mono

interface MccBlacklistService {
    fun addMccToBlacklist(mcc: MccBlacklistDto): Mono<MccBlacklistDto>

    fun deleteMccFromBlacklist(id: String): Mono<Int>

    fun getMccFromBlacklist(id: String): Mono<MccBlacklistDto>

    fun getMccFromBlacklistWithPagination(count: Int, page: Int): Mono<MccBlacklistPageDto>
}
