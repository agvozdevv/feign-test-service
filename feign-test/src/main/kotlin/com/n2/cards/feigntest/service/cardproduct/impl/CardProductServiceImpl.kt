package com.n2.cards.feigntest.service.cardproduct.impl

import com.n2.cards.cardproducts.feign.CardProductApi
import com.n2.cards.cardproducts.model.CardProductDto
import com.n2.cards.feigntest.service.cardproduct.api.CardProductService
import org.springframework.stereotype.Service
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono

@Service
class CardProductServiceImpl(private val cardProductApi: CardProductApi) : CardProductService {
    override fun createCardProduct(cardProduct: CardProductDto): Mono<CardProductDto> {
        return cardProductApi.createCardProduct(cardProduct)
    }

    override fun getAllCardProducts(): Flux<CardProductDto> {
        return cardProductApi.getAllCardProducts()
    }

    override fun updateCardProduct(id: String, cardProduct: CardProductDto): Mono<CardProductDto> {
        return cardProductApi.updateCardProduct(id, cardProduct)
    }

    override fun getCardProductById(id: String): Mono<CardProductDto> {
        return cardProductApi.getCardProductById(id)
    }
}
