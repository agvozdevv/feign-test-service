package com.n2.cards.feigntest.service.cardsantifraud.impl

import com.n2.cards.antifraud.feign.NetworkFraudConstraintApi
import com.n2.cards.antifraud.model.constraint.dto.NetworkFraudConstraintDto
import com.n2.cards.feigntest.service.cardsantifraud.api.NetworkFraudConstraintsService
import org.springframework.stereotype.Service
import reactor.core.publisher.Mono

@Service
class NetworkFraudConstraintsServiceImpl(
    private val networkFraudConstraintApi: NetworkFraudConstraintApi
) : NetworkFraudConstraintsService {
    override fun addNetworkFraudConstraint(constraint: NetworkFraudConstraintDto): Mono<NetworkFraudConstraintDto> {
        return networkFraudConstraintApi.addNetworkFraudConstraint(constraint)
    }

    override fun getLatestNetworkFraudConstraint(): Mono<NetworkFraudConstraintDto> {
        return networkFraudConstraintApi.getLatestNetworkFraudConstraint()
    }
}
