package com.n2.cards.feigntest.controller.cardsantifraud

import com.n2.cards.antifraud.model.blacklist.MerchantBlacklistPageDto
import com.n2.cards.antifraud.model.blacklist.dto.MerchantBlacklistDto
import com.n2.cards.feigntest.service.cardsantifraud.api.MerchantBlacklistService
import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.DeleteMapping
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.ResponseStatus
import org.springframework.web.bind.annotation.RestController
import reactor.core.publisher.Mono

@RestController
class MerchantBlacklistApiController(private val merchantBlacklistService: MerchantBlacklistService) {

    @PostMapping("/antifraud/blacklist/merchant")
    @ResponseStatus(HttpStatus.CREATED)
    fun addMerchantToBlacklist(@RequestBody merchant: MerchantBlacklistDto): Mono<MerchantBlacklistDto> {
        return merchantBlacklistService.addMerchantToBlacklist(merchant)
    }

    @DeleteMapping("/antifraud/blacklist/merchant/{id}")
    fun deleteMerchantFromBlacklist(@PathVariable id: String): Mono<Int> {
        return merchantBlacklistService.deleteMerchantFromBlacklist(id)
    }

    @GetMapping("/antifraud/blacklist/merchant/{id}")
    fun getMerchantFromBlacklist(@PathVariable id: String): Mono<MerchantBlacklistDto> {
        return merchantBlacklistService.getMerchantFromBlacklist(id)
    }

    @GetMapping("/antifraud/blacklist/merchant")
    fun getMerchantsFromBlacklistWithPagination(
        @RequestParam(required = false, defaultValue = "10") count: Int,
        @RequestParam(required = false, defaultValue = "0") page: Int,
    ): Mono<MerchantBlacklistPageDto> {
        return merchantBlacklistService.getMerchantsFromBlacklistWithPagination(count, page)
    }
}
