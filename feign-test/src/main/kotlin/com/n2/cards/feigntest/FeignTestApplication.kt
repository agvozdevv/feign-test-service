package com.n2.cards.feigntest

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class FeignTestApplication

fun main(args: Array<String>) {
    runApplication<FeignTestApplication>(*args)
}
