package com.n2.cards.feigntest.service.cardsantifraud.api

import com.n2.cards.antifraud.model.constraint.dto.NetworkFraudConstraintDto
import reactor.core.publisher.Mono

interface NetworkFraudConstraintsService {
    fun addNetworkFraudConstraint(constraint: NetworkFraudConstraintDto): Mono<NetworkFraudConstraintDto>

    fun getLatestNetworkFraudConstraint(): Mono<NetworkFraudConstraintDto>
}
