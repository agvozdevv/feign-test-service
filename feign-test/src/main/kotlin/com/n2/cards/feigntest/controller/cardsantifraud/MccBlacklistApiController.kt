package com.n2.cards.feigntest.controller.cardsantifraud

import com.n2.cards.antifraud.model.blacklist.MccBlacklistPageDto
import com.n2.cards.antifraud.model.blacklist.dto.MccBlacklistDto
import com.n2.cards.feigntest.service.cardsantifraud.api.MccBlacklistService
import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.DeleteMapping
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.ResponseStatus
import org.springframework.web.bind.annotation.RestController
import reactor.core.publisher.Mono

@RestController
class MccBlacklistApiController(private val mccBlacklistService: MccBlacklistService) {

    @PostMapping("/antifraud/blacklist/mcc")
    @ResponseStatus(HttpStatus.CREATED)
    fun addMccToBlacklist(@RequestBody mcc: MccBlacklistDto): Mono<MccBlacklistDto> {
        return mccBlacklistService.addMccToBlacklist(mcc)
    }

    @DeleteMapping("/antifraud/blacklist/mcc/{id}")
    fun deleteMccFromBlacklist(@PathVariable id: String): Mono<Int> {
        return mccBlacklistService.deleteMccFromBlacklist(id)
    }

    @GetMapping("/antifraud/blacklist/mcc/{id}")
    fun getMccFromBlacklist(@PathVariable id: String): Mono<MccBlacklistDto> {
        return mccBlacklistService.getMccFromBlacklist(id)
    }

    @GetMapping("/antifraud/blacklist/mcc")
    fun getMccFromBlacklistWithPagination(
        @RequestParam(required = false, defaultValue = "10") count: Int,
        @RequestParam(required = false, defaultValue = "0") page: Int,
    ): Mono<MccBlacklistPageDto> {
        return mccBlacklistService.getMccFromBlacklistWithPagination(count, page)
    }
}
