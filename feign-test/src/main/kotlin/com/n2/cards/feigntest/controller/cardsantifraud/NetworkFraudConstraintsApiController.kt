package com.n2.cards.feigntest.controller.cardsantifraud

import com.n2.cards.antifraud.model.constraint.dto.NetworkFraudConstraintDto
import com.n2.cards.feigntest.service.cardsantifraud.api.NetworkFraudConstraintsService
import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.ResponseStatus
import org.springframework.web.bind.annotation.RestController
import reactor.core.publisher.Mono

@RestController
class NetworkFraudConstraintsApiController(private val networkFraudConstraintsService: NetworkFraudConstraintsService) {
    @PostMapping("/antifraud/constraints/network")
    @ResponseStatus(HttpStatus.CREATED)
    fun addNetworkFraudConstraint(@RequestBody constraint: NetworkFraudConstraintDto): Mono<NetworkFraudConstraintDto> {
        return networkFraudConstraintsService.addNetworkFraudConstraint(constraint)
    }

    @GetMapping("/antifraud/constraints/network")
    fun getLatestNetworkFraudConstraint(): Mono<NetworkFraudConstraintDto> {
        return networkFraudConstraintsService.getLatestNetworkFraudConstraint()
    }
}
