package com.n2.cards.feigntest.service.cardproduct.api

import com.n2.cards.cardproducts.model.CardProductDto
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono

interface CardProductService {
    fun createCardProduct(cardProduct: CardProductDto): Mono<CardProductDto>

    fun getAllCardProducts(): Flux<CardProductDto>

    fun updateCardProduct(id: String, cardProduct: CardProductDto): Mono<CardProductDto>

    fun getCardProductById(id: String): Mono<CardProductDto>
}
