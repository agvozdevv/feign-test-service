package com.n2.cards.feigntest.controller.cardsantifraud

import com.n2.cards.antifraud.model.constraint.dto.IssuerFraudConstraintDto
import com.n2.cards.feigntest.service.cardsantifraud.api.IssuerFraudConstraintsService
import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.ResponseStatus
import org.springframework.web.bind.annotation.RestController
import reactor.core.publisher.Mono

@RestController
class IssuerFraudConstraintsApiController(private val issuerFraudConstraintsService: IssuerFraudConstraintsService) {
    @PostMapping("/antifraud/constraints/issuer")
    @ResponseStatus(HttpStatus.CREATED)
    fun addIssuerFraudConstraint(@RequestBody constraint: IssuerFraudConstraintDto): Mono<IssuerFraudConstraintDto> {
        return issuerFraudConstraintsService.addIssuerFraudConstraint(constraint)
    }

    @GetMapping("/antifraud/constraints/issuer")
    fun getLatestIssuerFraudConstraint(): Mono<IssuerFraudConstraintDto> {
        return issuerFraudConstraintsService.getLatestIssuerFraudConstraint()
    }
}
