package com.n2.cards.feigntest.service.cardrealmsgateway.impl

import com.n2.cards.cardrealmsgateway.feign.CardTransactionApi
import com.n2.cards.cardrealmsgateway.model.cardtx.TransactionDto
import com.n2.cards.feigntest.service.cardrealmsgateway.api.CardTxApiService
import org.springframework.stereotype.Service
import reactor.core.publisher.Mono

@Service
class CardTxApiServiceImpl(private val cardTransactionApi: CardTransactionApi) : CardTxApiService {
    override fun getTransactions(realmToken: String, transactionIdList: List<String>): Mono<List<TransactionDto>> {
        return cardTransactionApi.getTransactions(realmToken, transactionIdList)
    }

    override fun getTransactionById(transactionId: String, realmToken: String): Mono<TransactionDto> {
        return cardTransactionApi.getTransactionById(transactionId, realmToken)
    }
}
