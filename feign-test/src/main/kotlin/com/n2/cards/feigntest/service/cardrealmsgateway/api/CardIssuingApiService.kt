package com.n2.cards.feigntest.service.cardrealmsgateway.api

import com.n2.cards.cardrealmsgateway.model.cardissuing.AnswerGateway
import com.n2.cards.cardrealmsgateway.model.cardissuing.CardInfoResponse
import com.n2.cards.cardrealmsgateway.model.cardissuing.CreateCardRequestAddressAsObject
import com.n2.cards.cardrealmsgateway.model.cardissuing.CreateCardRequestAddressAsString
import com.n2.cards.cardrealmsgateway.model.cardissuing.GatewayPinRequest
import com.n2.cards.cardrealmsgateway.model.cardissuing.N2CardResponseGateway
import reactor.core.publisher.Mono

interface CardIssuingApiService {
    fun getCardRequisitesById(cardId: String, realmToken: String): Mono<CardInfoResponse>

    fun setPinToCard(cardId: String, realmToken: String, request: GatewayPinRequest): Mono<AnswerGateway>

    fun createCardAddressAsObject(
        realmUserId: String,
        realmToken: String,
        request: CreateCardRequestAddressAsObject
    ): Mono<N2CardResponseGateway>

    fun createCardAddressAsString(
        realmUserId: String,
        realmToken: String,
        request: CreateCardRequestAddressAsString
    ): Mono<N2CardResponseGateway>

    fun reissueCard(oldCardId: String, realmToken: String): Mono<String>
}
