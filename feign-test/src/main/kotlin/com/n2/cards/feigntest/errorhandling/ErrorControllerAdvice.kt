package com.n2.cards.feigntest.errorhandling

import feign.FeignException
import org.slf4j.LoggerFactory
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.ExceptionHandler
import org.springframework.web.bind.annotation.RestControllerAdvice

@RestControllerAdvice
class ErrorControllerAdvice {
    private val logger = LoggerFactory.getLogger(javaClass)

    @ExceptionHandler(FeignException::class)
    fun handleException(exception: RuntimeException): ResponseEntity<Map<String, String?>> {
        logger.info(exception.message)

        val errorMap = mapOf(
            "Error message" to "Feign exception occured",
            "Status" to HttpStatus.BAD_REQUEST.toString()
        )

        return ResponseEntity.ok(errorMap)
    }
}
