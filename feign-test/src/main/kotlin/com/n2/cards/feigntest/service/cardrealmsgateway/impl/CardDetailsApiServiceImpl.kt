package com.n2.cards.feigntest.service.cardrealmsgateway.impl

import com.n2.cards.cardrealmsgateway.feign.CardDetailsApi
import com.n2.cards.cardrealmsgateway.model.carddetails.CardInfoDto
import com.n2.cards.cardrealmsgateway.model.carddetails.PinResponse
import com.n2.cards.feigntest.service.cardrealmsgateway.api.CardDetailsApiService
import org.springframework.stereotype.Service
import reactor.core.publisher.Mono

@Service
class CardDetailsApiServiceImpl(private val cardDetailsApi: CardDetailsApi) : CardDetailsApiService {
    override fun checkCardForPinExisting(cardId: String, realmToken: String): Mono<PinResponse> {
        return cardDetailsApi.checkCardForPinExisting(cardId, realmToken)
    }

    override fun getCardDetailsForCardId(cardId: String, realmToken: String): Mono<CardInfoDto> {
        return cardDetailsApi.getCardDetailsForCardId(cardId, realmToken)
    }
}
