package com.n2.cards.feigntest.controller.cardrealmsgateway

import com.n2.cards.cardrealmsgateway.model.cardissuing.AnswerGateway
import com.n2.cards.cardrealmsgateway.model.cardissuing.CardInfoResponse
import com.n2.cards.cardrealmsgateway.model.cardissuing.CreateCardRequestAddressAsObject
import com.n2.cards.cardrealmsgateway.model.cardissuing.CreateCardRequestAddressAsString
import com.n2.cards.cardrealmsgateway.model.cardissuing.GatewayPinRequest
import com.n2.cards.cardrealmsgateway.model.cardissuing.N2CardResponseGateway
import com.n2.cards.feigntest.service.cardrealmsgateway.api.CardIssuingApiService
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestHeader
import org.springframework.web.bind.annotation.RestController
import reactor.core.publisher.Mono

@RestController
class CardIssuingApiController(private val cardIssuingApiService: CardIssuingApiService) {

    @GetMapping("/card-issuing/cards/{cardId}/requisites")
    fun getCardRequisitesById(
        @PathVariable("cardId") cardId: String,
        @RequestHeader("realm-token") realmToken: String
    ): Mono<CardInfoResponse> {
        return cardIssuingApiService.getCardRequisitesById(cardId, realmToken)
    }

    @PostMapping("/card-issuing/cards/{cardId}/pin")
    fun setPinToCard(
        @PathVariable("cardId") cardId: String,
        @RequestHeader("realm-token") realmToken: String,
        @RequestBody request: GatewayPinRequest
    ): Mono<AnswerGateway> {
        return cardIssuingApiService.setPinToCard(cardId, realmToken, request)
    }

    @PostMapping("/card-issuing/users/{realmUserId}/cards")
    fun createCardAddressAsObject(
        @PathVariable("realmUserId") realmUserId: String,
        @RequestHeader("realm-token") realmToken: String,
        @RequestBody request: CreateCardRequestAddressAsObject
    ): Mono<N2CardResponseGateway> {
        return cardIssuingApiService.createCardAddressAsObject(realmUserId, realmToken, request)
    }

    @PostMapping("/card-issuing/users/{realmUserId}/cards/address-as-string")
    fun createCardAddressAsString(
        @PathVariable("realmUserId") realmUserId: String,
        @RequestHeader("realm-token") realmToken: String,
        @RequestBody request: CreateCardRequestAddressAsString
    ): Mono<N2CardResponseGateway> {
        return cardIssuingApiService.createCardAddressAsString(realmUserId, realmToken, request)
    }

    @PostMapping("/card-issuing/cards/{oldCardId}/reissue")
    fun reissueCard(
        @PathVariable("oldCardId") oldCardId: String,
        @RequestHeader("realm-token") realmToken: String,
    ): Mono<String> {
        return cardIssuingApiService.reissueCard(oldCardId, realmToken)
    }
}
