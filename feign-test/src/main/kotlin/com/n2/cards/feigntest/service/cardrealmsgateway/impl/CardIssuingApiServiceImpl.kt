package com.n2.cards.feigntest.service.cardrealmsgateway.impl

import com.n2.cards.cardrealmsgateway.feign.CardIssuingApi
import com.n2.cards.cardrealmsgateway.model.cardissuing.AnswerGateway
import com.n2.cards.cardrealmsgateway.model.cardissuing.CardInfoResponse
import com.n2.cards.cardrealmsgateway.model.cardissuing.CreateCardRequestAddressAsObject
import com.n2.cards.cardrealmsgateway.model.cardissuing.CreateCardRequestAddressAsString
import com.n2.cards.cardrealmsgateway.model.cardissuing.GatewayPinRequest
import com.n2.cards.cardrealmsgateway.model.cardissuing.N2CardResponseGateway
import com.n2.cards.feigntest.service.cardrealmsgateway.api.CardIssuingApiService
import org.springframework.stereotype.Service
import reactor.core.publisher.Mono

@Service
class CardIssuingApiServiceImpl(private val cardIssuingApi: CardIssuingApi) : CardIssuingApiService {
    override fun getCardRequisitesById(cardId: String, realmToken: String): Mono<CardInfoResponse> {
        return cardIssuingApi.getCardRequisitesById(cardId, realmToken)
    }

    override fun setPinToCard(cardId: String, realmToken: String, request: GatewayPinRequest): Mono<AnswerGateway> {
        return cardIssuingApi.setPinToCard(cardId, realmToken, request)
    }

    override fun createCardAddressAsObject(
        realmUserId: String,
        realmToken: String,
        request: CreateCardRequestAddressAsObject
    ): Mono<N2CardResponseGateway> {
        return cardIssuingApi.createCardAddressAsObject(realmUserId, realmToken, request)
    }

    override fun createCardAddressAsString(
        realmUserId: String,
        realmToken: String,
        request: CreateCardRequestAddressAsString
    ): Mono<N2CardResponseGateway> {
        return cardIssuingApi.createCardAddressAsString(realmUserId, realmToken, request)
    }

    override fun reissueCard(oldCardId: String, realmToken: String): Mono<String> {
        return cardIssuingApi.reissueCard(oldCardId, realmToken)
    }
}
