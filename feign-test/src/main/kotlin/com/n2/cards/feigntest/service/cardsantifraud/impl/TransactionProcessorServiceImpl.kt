package com.n2.cards.feigntest.service.cardsantifraud.impl

import com.n2.cards.antifraud.feign.TransactionProcessorApi
import com.n2.cards.antifraud.model.ProcessingResult
import com.n2.cards.feigntest.service.cardsantifraud.api.TransactionProcessorService
import org.springframework.stereotype.Service
import reactor.core.publisher.Mono

@Service
class TransactionProcessorServiceImpl(
    private val transactionProcessorApi: TransactionProcessorApi
) : TransactionProcessorService {
    override fun fraudCheck(request: String): Mono<ProcessingResult> {
        return transactionProcessorApi.fraudCheck(request)
    }
}
