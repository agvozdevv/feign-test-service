package com.n2.cards.feigntest.service.cardsantifraud.impl

import com.n2.cards.antifraud.feign.MccBlacklistApi
import com.n2.cards.antifraud.model.blacklist.MccBlacklistPageDto
import com.n2.cards.antifraud.model.blacklist.dto.MccBlacklistDto
import com.n2.cards.feigntest.service.cardsantifraud.api.MccBlacklistService
import org.springframework.stereotype.Service
import reactor.core.publisher.Mono

@Service
class MccBlacklistServiceImpl(private val mccBlacklistApi: MccBlacklistApi) : MccBlacklistService {
    override fun addMccToBlacklist(mcc: MccBlacklistDto): Mono<MccBlacklistDto> {
        return mccBlacklistApi.addMccToBlacklist(mcc)
    }

    override fun deleteMccFromBlacklist(id: String): Mono<Int> {
        return mccBlacklistApi.deleteMccFromBlacklist(id)
    }

    override fun getMccFromBlacklist(id: String): Mono<MccBlacklistDto> {
        return mccBlacklistApi.getMccFromBlacklist(id)
    }

    override fun getMccFromBlacklistWithPagination(count: Int, page: Int): Mono<MccBlacklistPageDto> {
        return mccBlacklistApi.getMccFromBlacklistWithPagination(count, page)
    }
}
