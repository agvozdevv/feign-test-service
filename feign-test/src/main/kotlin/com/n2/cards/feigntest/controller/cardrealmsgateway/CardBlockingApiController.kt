package com.n2.cards.feigntest.controller.cardrealmsgateway

import com.n2.cards.cardrealmsgateway.model.cardblocking.CardBlockingRequestGateway
import com.n2.cards.cardrealmsgateway.model.cardblocking.CardBlockingResponseGateway
import com.n2.cards.feigntest.service.cardrealmsgateway.api.CardBlockingApiService
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PutMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestHeader
import org.springframework.web.bind.annotation.RestController
import reactor.core.publisher.Mono

@RestController
class CardBlockingApiController(private val cardBlockingApiService: CardBlockingApiService) {

    @PutMapping("/card-blocking/cards/{cardId}/block")
    fun blockCard(
        @PathVariable("cardId") cardId: String,
        @RequestHeader("realm-token") realmToken: String,
        @RequestBody request: CardBlockingRequestGateway
    ): Mono<CardBlockingResponseGateway> {
        return cardBlockingApiService.blockCard(cardId, realmToken, request)
    }
}
