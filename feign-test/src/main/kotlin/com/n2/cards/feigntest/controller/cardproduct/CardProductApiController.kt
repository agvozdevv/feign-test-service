package com.n2.cards.feigntest.controller.cardproduct

import com.n2.cards.cardproducts.model.CardProductDto
import com.n2.cards.feigntest.service.cardproduct.api.CardProductService
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.PutMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RestController
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono

@RestController
class CardProductApiController(private val cardProductService: CardProductService) {

    @PostMapping("/card-products")
    fun createCardProduct(@RequestBody cardProduct: CardProductDto): Mono<CardProductDto> {
        return cardProductService.createCardProduct(cardProduct)
    }

    @GetMapping("/card-products")
    fun getAllCardProducts(): Flux<CardProductDto> {
        return cardProductService.getAllCardProducts()
    }

    @PutMapping("/card-products/{id}")
    fun updateCardProduct(
        @PathVariable("id") id: String,
        @RequestBody cardProduct: CardProductDto
    ): Mono<CardProductDto> {
        return cardProductService.updateCardProduct(id, cardProduct)
    }

    @GetMapping("/card-products/{id}")
    fun getCardProductById(@PathVariable("id") id: String): Mono<CardProductDto> {
        return cardProductService.getCardProductById(id)
    }
}
