package com.n2.cards.feigntest.service.cardrealmsgateway.api

import com.n2.cards.cardrealmsgateway.model.cardssca.RequestDto
import com.n2.cards.cardrealmsgateway.model.cardssca.ThreeDSAuthenticationResultRequest
import com.n2.cards.cardrealmsgateway.model.cardssca.ThreeDSAuthenticationResultResponse
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono

interface CardsScaApiService {
    fun getRequestsForCardIdList(realmToken: String, cardIdList: List<String>): Flux<RequestDto>

    fun confirmRequest(
        requestId: String,
        realmToken: String,
        request: ThreeDSAuthenticationResultRequest
    ): Mono<ThreeDSAuthenticationResultResponse>
}
