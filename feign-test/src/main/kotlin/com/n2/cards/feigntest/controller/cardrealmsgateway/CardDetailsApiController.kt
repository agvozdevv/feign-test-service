package com.n2.cards.feigntest.controller.cardrealmsgateway

import com.n2.cards.cardrealmsgateway.model.carddetails.CardInfoDto
import com.n2.cards.cardrealmsgateway.model.carddetails.PinResponse
import com.n2.cards.feigntest.service.cardrealmsgateway.api.CardDetailsApiService
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestHeader
import org.springframework.web.bind.annotation.RestController
import reactor.core.publisher.Mono

@RestController
class CardDetailsApiController(private val cardDetailsApiService: CardDetailsApiService) {

    @GetMapping("/card-details/cards/{cardId}/pin")
    fun checkCardForPinExisting(
        @PathVariable("cardId") cardId: String,
        @RequestHeader("realm-token") realmToken: String
    ): Mono<PinResponse> {
        return cardDetailsApiService.checkCardForPinExisting(cardId, realmToken)
    }

    @GetMapping("/card-details/cards/{cardId}")
    fun getCardDetailsForCardId(
        @PathVariable("cardId") cardId: String,
        @RequestHeader("realm-token") realmToken: String
    ): Mono<CardInfoDto> {
        return cardDetailsApiService.getCardDetailsForCardId(cardId, realmToken)
    }
}
