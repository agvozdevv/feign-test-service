package com.n2.cards.feigntest.service.cardrealmsgateway.api

import com.n2.cards.cardrealmsgateway.model.carddetails.CardInfoDto
import com.n2.cards.cardrealmsgateway.model.carddetails.PinResponse
import reactor.core.publisher.Mono

interface CardDetailsApiService {
    fun checkCardForPinExisting(cardId: String, realmToken: String): Mono<PinResponse>

    fun getCardDetailsForCardId(cardId: String, realmToken: String): Mono<CardInfoDto>
}
