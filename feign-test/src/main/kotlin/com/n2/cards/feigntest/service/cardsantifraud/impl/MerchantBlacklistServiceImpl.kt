package com.n2.cards.feigntest.service.cardsantifraud.impl

import com.n2.cards.antifraud.feign.MerchantBlacklistApi
import com.n2.cards.antifraud.model.blacklist.MerchantBlacklistPageDto
import com.n2.cards.antifraud.model.blacklist.dto.MerchantBlacklistDto
import com.n2.cards.feigntest.service.cardsantifraud.api.MerchantBlacklistService
import org.springframework.stereotype.Service
import reactor.core.publisher.Mono

@Service
class MerchantBlacklistServiceImpl(private val merchantBlacklistApi: MerchantBlacklistApi) : MerchantBlacklistService {
    override fun addMerchantToBlacklist(merchant: MerchantBlacklistDto): Mono<MerchantBlacklistDto> {
        return merchantBlacklistApi.addMerchantToBlacklist(merchant)
    }

    override fun deleteMerchantFromBlacklist(id: String): Mono<Int> {
        return merchantBlacklistApi.deleteMerchantFromBlacklist(id)
    }

    override fun getMerchantFromBlacklist(id: String): Mono<MerchantBlacklistDto> {
        return merchantBlacklistApi.getMerchantFromBlacklist(id)
    }

    override fun getMerchantsFromBlacklistWithPagination(count: Int, page: Int): Mono<MerchantBlacklistPageDto> {
        return merchantBlacklistApi.getMerchantsFromBlacklistWithPagination(count, page)
    }
}
