package com.n2.cards.feigntest.service.cardrealmsgateway.impl

import com.n2.cards.cardrealmsgateway.feign.CardBlockingApi
import com.n2.cards.cardrealmsgateway.model.cardblocking.CardBlockingRequestGateway
import com.n2.cards.cardrealmsgateway.model.cardblocking.CardBlockingResponseGateway
import com.n2.cards.feigntest.service.cardrealmsgateway.api.CardBlockingApiService
import org.springframework.stereotype.Service
import reactor.core.publisher.Mono

@Service
class CardBlockingApiServiceImpl(private val cardBlockingApi: CardBlockingApi) : CardBlockingApiService {
    override fun blockCard(
        cardId: String,
        realmToken: String,
        request: CardBlockingRequestGateway
    ): Mono<CardBlockingResponseGateway> {
        return cardBlockingApi.blockCard(cardId, realmToken, request)
    }
}
